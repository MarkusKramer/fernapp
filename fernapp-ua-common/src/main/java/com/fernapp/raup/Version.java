package com.fernapp.raup;

/**
 * @author Markus
 */
public final class Version {

	public final static int RAUP_PROTOCOL_VERSION = 2;

}
