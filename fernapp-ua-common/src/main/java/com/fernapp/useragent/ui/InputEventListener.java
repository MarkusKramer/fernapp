package com.fernapp.useragent.ui;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * Listener for input events on a remote window. The events are related to the content area/panel not the surrounding
 * window container.
 * 
 * @author Markus
 */
public interface InputEventListener {

	/**
	 * Invoked when a key has been pressed. See the class description for {@link KeyEvent} for a definition of a key
	 * pressed event.
	 */
	public void keyPressed(KeyEvent e);

	/**
	 * Invoked when a key has been released. See the class description for {@link KeyEvent} for a definition of a key
	 * released event.
	 */
	public void keyReleased(KeyEvent e);

	/**
	 * Invoked when a mouse button has been pressed on a component.
	 * 
	 * @param button
	 *            see {@link MouseEvent}
	 */
	public void mousePressed(int button);

	/**
	 * Invoked when a mouse button has been released on a component.
	 * 
	 * @param button
	 *            see {@link MouseEvent}
	 */
	public void mouseReleased(int button);

	/**
	 * Invoked when the mouse cursor has been moved onto a component but no buttons have been pushed.
	 */
	public void mouseMoved(int x, int y);

}
