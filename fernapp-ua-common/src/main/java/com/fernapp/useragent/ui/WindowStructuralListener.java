package com.fernapp.useragent.ui;

/**
 * Listener that informs about structural changes to a {@link WindowContainer}.
 * 
 * @author Markus
 */
public interface WindowStructuralListener {

	/**
	 * A resize has happened. Contains the information if the resize was caused by the user or was due to a software
	 * call. May be called many times and sequence (information will be compressed).
	 */
	void resized(int width, int height, boolean causedByUser);

	/**
	 * The user wants to close the window. This only indicates the wish of the user, the window itself remains open.
	 */
	void closing();

}
