package com.fernapp.useragent;

/**
 * Created by the VM, passed via mgmt to the user agent. Used by the user agent to connect to the VM.
 * 
 * @author Markus
 */
public class ApplicationSessionConnectionDetails {

	private String host;
	private int port;

	public ApplicationSessionConnectionDetails(String host, int port) {
		this.host = host;
		this.port = port;
	}

	@Override
	public String toString() {
		return "[host=" + host + ", port=" + port + "]";
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
