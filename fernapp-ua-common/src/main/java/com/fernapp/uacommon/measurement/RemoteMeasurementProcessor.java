package com.fernapp.uacommon.measurement;

import com.fernapp.raup.measurement.RemoteMeasurements;
import com.fernapp.uacommon.middleware.MessagingConnection;

/**
 * @author Markus
 * 
 */
public class RemoteMeasurementProcessor implements MeasurementProcessor {

	private MessagingConnection messagingConnection;

	public RemoteMeasurementProcessor(MessagingConnection messagingConnection) {
		this.messagingConnection = messagingConnection;
	}

	/**
	 * @see com.fernapp.uacommon.measurement.MeasurementProcessor#processMeasurement(com.fernapp.uacommon.measurement.Measurement)
	 */
	public void processMeasurement(Measurement measurement) {
		RemoteMeasurements rms = new RemoteMeasurements(measurement);
		messagingConnection.sendMessageAsync(rms);
	}

}
