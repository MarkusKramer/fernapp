package com.fernapp.uacommon.measurement;

/**
 * @author Markus
 * 
 */
public interface MeasurementReceiver {

	void receiveMeasurement(DelayCategory delayCategory, long delay, long dataSize);

	void receiveMeasurement(Measurement measurement);

}
