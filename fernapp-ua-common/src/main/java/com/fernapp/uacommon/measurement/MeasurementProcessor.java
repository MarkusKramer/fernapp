package com.fernapp.uacommon.measurement;

/**
 * @author Markus
 * 
 */
public interface MeasurementProcessor {

	void processMeasurement(Measurement measurement);

}
