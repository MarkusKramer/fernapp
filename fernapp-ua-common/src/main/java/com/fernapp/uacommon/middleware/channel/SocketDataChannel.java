package com.fernapp.uacommon.middleware.channel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.AccessControlException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.util.NamedThreadFactory;

/**
 * @author Markus
 * 
 */
public class SocketDataChannel implements DataChannel {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private volatile boolean open = true;
	private final Socket socket;
	private final ExecutorService writeExecutorService;

	public SocketDataChannel(Socket socket) {
		this.socket = socket;
		writeExecutorService = Executors.newFixedThreadPool(1, new NamedThreadFactory("socketWriter"
				+ getRemoteEndpointDescriptor()));

		try {
			if (log.isDebugEnabled()) {
				log.debug("Socket receive buffer " + socket.getReceiveBufferSize() + ", send buffer:"
						+ socket.getSendBufferSize());
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.channel.DataChannel#getRemoteEndpointDescriptor()
	 */
	public String getRemoteEndpointDescriptor() {
		return socket.getRemoteSocketAddress().toString();
	}

	/**
	 * @see java.nio.channels.Channel#isOpen()
	 */
	public synchronized boolean isOpen() {
		return open;
	}

	/**
	 * @see java.nio.channels.Channel#close()
	 */
	public synchronized void close() throws IOException {
		if (open) {
			open = false;
			try {
				try {
					// doesn't work in applets
					writeExecutorService.shutdown();
				} catch (AccessControlException e1) {
					log.error("Unable to shutdown thread pool", e1);
				}
				socket.close();
			} catch (IOException e) {
				log.warn("Connection close had a problem", e);
			}
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.channel.DataChannel#getInputStream()
	 */
	public InputStream getInputStream() {
		try {
			return socket.getInputStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.channel.DataChannel#getOutputStream()
	 */
	public OutputStream getOutputStream() {
		try {
			return socket.getOutputStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.channel.DataChannel#write(com.fernapp.uacommon.middleware.channel.DataChannel.WriteJob,
	 *      long, java.util.concurrent.TimeUnit)
	 */
	public void write(final WriteJob writeJob, long timeout, TimeUnit unit) throws TimeoutException, IOException {
		// detect a closed socket by using a write timeout
		// http://stackoverflow.com/questions/983009/java-groovy-socket-write-timeout

		Callable<Void> sendCallable = new Callable<Void>() {
			public Void call() throws IOException {
				writeJob.write();
				return null;
			}
		};
		Future<Void> writeFuture = writeExecutorService.submit(sendCallable);
		try {
			// FIXME if several writes are queued this might timeout too early
			writeFuture.get(timeout, unit);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} catch (ExecutionException e) {
			if (e.getCause() instanceof IOException) {
				IOException root = (IOException) e.getCause();
				throw new IOException("Exception during write job", root);
			} else {
				throw new RuntimeException(e);
			}
		}
	}

}
