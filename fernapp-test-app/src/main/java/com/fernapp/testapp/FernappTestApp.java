package com.fernapp.testapp;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.WindowConstants;

/**
 * A swing application on which tests can be performed on.
 * 
 * @author Markus
 */
public class FernappTestApp extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPopupMenu popupMenu = new JPopupMenu();

	private int boxX = 0;
	private int boxY = 50;

	public FernappTestApp() {
		setTitle("Fernapp Testing Application");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(300, 200));
		setContentPane(new ImagePanel());

		addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				moveObject();
			}
		});

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				System.out.println("Resized to width " + e.getComponent().getWidth() + ", height "
						+ e.getComponent().getHeight());
				super.componentResized(e);
			}
		});

		registerContextMenu();
		pack();
		System.out.println("Showing test application");
		setVisible(true);
	}

	private void moveObject() {
		boxX += 10;

		if (boxX > getWidth()) {
			boxX = 0;
		}
		repaint();
	}

	private class ImagePanel extends JPanel {

		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
			System.out.println("Repainting panel");
			super.paint(g);

			// background
			g.setColor(Color.YELLOW);
			g.fillRect(0, 0, getWidth(), getHeight());

			// paint box
			g.setColor(Color.BLACK);
			g.fillRect(boxX, boxY, 50, 50);
		}
	}

	public static void main(String[] args) {
		new FernappTestApp();
	}

	private void registerContextMenu() {
		ActionListener al = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(FernappTestApp.this, ((JMenuItem) e.getSource()).getText());
			}
		};

		JMenuItem m;
		m = new JMenuItem("item1");
		m.addActionListener(al);
		popupMenu.add(m);
		m = new JMenuItem("item2");
		m.addActionListener(al);
		popupMenu.add(m);

		JMenu submenu = new JMenu("item3");
		m = new JMenuItem("subitem1");
		m.addActionListener(al);
		submenu.add(m);
		m = new JMenuItem("subitem2");
		m.addActionListener(al);
		submenu.add(m);
		submenu.addActionListener(al);
		popupMenu.add(submenu);

		PopupListener pl = new PopupListener();
		addMouseListener(pl);
	}

	class PopupListener extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			maybeShowPopup(e);
		}

		public void mouseReleased(MouseEvent e) {
			maybeShowPopup(e);
		}

		private void maybeShowPopup(MouseEvent e) {
			if (e.isPopupTrigger())
				popupMenu.show(FernappTestApp.this, e.getX(), e.getY());
		}
	}

}
