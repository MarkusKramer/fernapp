#include <stdlib.h>
#include <signal.h>
#include "jniConnector.h"


JavaVM *jvm;


void throwException(JNIEnv *env, const char *msg) {
	jclass cls = (*env)->FindClass(env, "java/lang/RuntimeException");
	(*env)->ThrowNew(env, cls, msg);
	(*env)->DeleteLocalRef(env, cls);
}


JNIEnv *getJniEnv() {
	JNIEnv *env;
	if( jvm == NULL || (*jvm)->GetEnv(jvm, (void **)&env, JNI_VERSION_1_2) ) {
		return NULL;
	}
	return env;
}


void jni_log(int logLevel, char* message) {
	JNIEnv *env = getJniEnv();

    jstring msgStr = (*env)->NewStringUTF(env, message);
	(*env)->CallStaticVoidMethod(env, class_X11ApplicationExecutor, method_logDebug, msgStr, (jint) logLevel);
	(*env)->DeleteLocalRef(env, msgStr);

	handlePotentialJniErrors(env);
}


void initJni(JNIEnv *env, jobject x11ApplicationExecutor) {
	// init JNI IDs
	class_X11ApplicationExecutor = (*env)->FindClass(env, "com/fernapp/server/executor/X11ApplicationExecutor");
	class_WindowEventListener = (*env)->FindClass(env, "com/fernapp/server/executor/WindowEventListener");
	class_VideoStreamChunk = (*env)->FindClass(env, "com/fernapp/raup/windowmanagement/server/VideoStreamChunk");
	class_WindowContent = (*env)->FindClass(env, "com/fernapp/server/encoding/WindowContent");
	class_WindowSettings = (*env)->FindClass(env, "com/fernapp/raup/windowmanagement/server/WindowSettings");
	class_ArrayList = (*env)->FindClass(env, "java/util/ArrayList");
	class_DamageReport = (*env)->FindClass(env, "com/fernapp/server/encoding/DamageReport");

	method_logDebug = (*env)->GetStaticMethodID(env, class_X11ApplicationExecutor, "_logDebug", "(Ljava/lang/String;I)V");
	method_receiveNativeMeasurement = (*env)->GetMethodID(env, class_X11ApplicationExecutor, "receiveNativeMeasurement", "(IJJ)V");
	method_WindowEventListener_onWindowSettingsChange = (*env)->GetMethodID(env, class_WindowEventListener, "onWindowSettingsChange", "(Lcom/fernapp/raup/windowmanagement/server/WindowSettings;Z)V");
	method_WindowEventListener_onWindowDestroyed = (*env)->GetMethodID(env, class_WindowEventListener, "onWindowDestroyed", "(Ljava/lang/String;)V");
	method_WindowEventListener_onWindowContentUpdate = (*env)->GetMethodID(env, class_WindowEventListener, "onWindowContentUpdate", "(Ljava/lang/String;Lcom/fernapp/server/encoding/DamageReport;)V");
	method_streamChunkClass_ctor = (*env)->GetMethodID(env, class_VideoStreamChunk, "<init>", "(IIIZ[B)V");
	method_windowContent_ctor = (*env)->GetMethodID(env, class_WindowContent, "<init>", "(J)V");
	method_windowSettings_ctor = (*env)->GetMethodID(env, class_WindowSettings, "<init>", "(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;II)V");
	method_ArrayList_ctor = (*env)->GetMethodID(env, class_ArrayList, "<init>", "()V");
	method_ArrayList_add = (*env)->GetMethodID(env, class_ArrayList, "add", "(Ljava/lang/Object;)Z");
	method_DamageReport_ctor = (*env)->GetMethodID(env, class_DamageReport, "<init>", "(IIII)V");
	method_DamageReport_getX = (*env)->GetMethodID(env, class_DamageReport, "getX", "()I");
	method_DamageReport_getY = (*env)->GetMethodID(env, class_DamageReport, "getY", "()I");
	method_DamageReport_getWidth = (*env)->GetMethodID(env, class_DamageReport, "getWidth", "()I");
	method_DamageReport_getHeight = (*env)->GetMethodID(env, class_DamageReport, "getHeight", "()I");

	// get WindowEventListener object
	jmethodID method_getWindowEventListener = (*env)->GetMethodID(env, class_X11ApplicationExecutor, "getWindowEventListener", "()Lcom/fernapp/server/executor/WindowEventListener;");
	object_WindowEventListener = (*env)->CallObjectMethod(env, x11ApplicationExecutor, method_getWindowEventListener);
	if(!object_WindowEventListener) {
		throwException(env, "object_WindowEventListener is null");
	}

	// wrap it into global references so it won't be garbage collected
	class_X11ApplicationExecutor = (*env)->NewGlobalRef(env, class_X11ApplicationExecutor);
	class_WindowEventListener = (*env)->NewGlobalRef(env, class_WindowEventListener);
	class_VideoStreamChunk = (*env)->NewGlobalRef(env, class_VideoStreamChunk);
	class_WindowContent = (*env)->NewGlobalRef(env, class_WindowContent);
	class_WindowSettings = (*env)->NewGlobalRef(env, class_WindowSettings);
	class_ArrayList = (*env)->NewGlobalRef(env, class_ArrayList);
	class_DamageReport = (*env)->NewGlobalRef(env, class_DamageReport);
	object_WindowEventListener = (*env)->NewGlobalRef(env, object_WindowEventListener);
	object_X11ApplicationExecutor = (*env)->NewGlobalRef(env, x11ApplicationExecutor);

	handlePotentialJniErrors(env);

	// native logging
	_log = jni_log;
}


JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *_jvm, void *reserved) {
	jvm = _jvm;
	return JNI_VERSION_1_2;
}


WindowId parseWindowIdString(JNIEnv *env, jstring windowIdString) {
	if(windowIdString != NULL) {
		const char *str = (*env)->GetStringUTFChars(env, windowIdString, NULL);
		if (str == NULL) {
			logError("GetStringUTFChars failed");
			return -1; /* OutOfMemoryError already thrown */
		}

		unsigned long wId = strtoul(str, NULL, 10);
		(*env)->ReleaseStringUTFChars(env, windowIdString, str);

		return wId;
	} else {
		logError("Window ID is null");
		return 0;
	}
}


jstring buildWindowIdString(JNIEnv *env, WindowId windowId) {
	char strBuffer[20];
	sprintf(strBuffer, "%lu", windowId);

    jstring windowIdString = (*env)->NewStringUTF(env, strBuffer);
    return windowIdString;
}


void handlePotentialJniErrors(JNIEnv *env) {
	if( (*env)->ExceptionOccurred(env) ) {
		// in case of a "jni call made with exception pending" error, this will give the error details
		(*env)->ExceptionDescribe(env);
		(*env)->ExceptionClear(env);

		// we cannot log a normal error message because this might trigger more JNI faults
		printf("JNI had a fatal error. Will go dying now.");
		// crash the program to get a stack trace by the JVM
		raise(SIGSEGV);
	}
}


jobject encode(JNIEnv *env, jobject instance, jlong windowContentPointer, jlong encContext,
		uint8_t *(*encodeFunction)(WindowContent *wc, DamageReport damageReport, int *dataSize, Bool *reset, void *ctx), int encodingType, jobject damageReportObject) {

	void *encoderContext = (void*)(intptr_t) encContext;
	WindowContent *wc = (void*)(intptr_t) windowContentPointer;

	// Upon encoder initialization damage information is not available (or userless) -> encode needs to encode everything upon initialization.
	// Furthermore, by the time we get here other areas might have damage as well or the size of the window has changed and this damage report is invalid.
	// Encoder implementations have to deal with this!
	DamageReport damageReport;
	if( damageReportObject != NULL ) {
		damageReport.damageX = (*env)->CallIntMethod(env, damageReportObject, method_DamageReport_getX);
		damageReport.damageY = (*env)->CallIntMethod(env, damageReportObject, method_DamageReport_getY);
		damageReport.damageWidth = (*env)->CallIntMethod(env, damageReportObject, method_DamageReport_getWidth);
		damageReport.damageHeight = (*env)->CallIntMethod(env, damageReportObject, method_DamageReport_getHeight);
	} else {
		damageReport.damageX = 0;
		damageReport.damageY = 0;
		damageReport.damageWidth = wc->width;
		damageReport.damageHeight = wc->height;
	}

	int dataSize;
	Bool reset;
	uint8_t *data = encodeFunction(wc, damageReport, &dataSize, &reset, encoderContext);

	jobject streamChunk = NULL;
	if( data != NULL ) {
		// build VideoStreamChunk object
		// TODO Copying arrays and strings instead of pinning them down can degrade performance
		// http://janet-project.sourceforge.net/papers/jnibench.pdf
		jsize arrSize = (long) dataSize;
		jbyteArray dataArr = (*env)->NewByteArray(env, arrSize);
		(*env)->SetByteArrayRegion(env, dataArr, 0, dataSize, (signed char*)data);
		streamChunk = (*env)->NewObject(env, class_VideoStreamChunk, method_streamChunkClass_ctor, wc->width, wc->height, encodingType, reset, dataArr);
		(*env)->DeleteLocalRef(env, dataArr);
		free(data);
	}

	freeWindowContent(wc);
	return streamChunk;
}
