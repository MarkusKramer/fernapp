#include "encoderUtil.h"
#include "mpngEncoder.h"
#include <png.h>


typedef struct MpngEncoderContext_type {
	int width;
	int height;
} MpngEncoderContext;


typedef struct PngEncoderBuffer_type {
	uint8_t *data;
	int size;
	int position;
} PngEncoderBuffer;


void *mpngEncoderInit() {
	MpngEncoderContext *ctx = (MpngEncoderContext*) malloc(sizeof(MpngEncoderContext));
	ctx->width = 0;
	return ctx;
}


void pngWriteData(png_structp png_ptr, png_bytep data, png_size_t length) {
	PngEncoderBuffer* buffer = (PngEncoderBuffer*) png_ptr->io_ptr;

	// grow buffer if necessary
	if (buffer->position + length >= buffer->size) {
		buffer->size = buffer->size * 2;
		buffer->data = realloc(buffer->data, buffer->size);
	}

	// copy new bytes to end of buffer
	ASSERT(buffer->data != NULL);
	memcpy_wrapper(buffer->data + buffer->position, data, length);
	buffer->position += length;
}


void writeInt(int data, PngEncoderBuffer *buffer) {
	memcpy_wrapper(buffer->data + buffer->position, &data, sizeof(int));
	buffer->position += sizeof(int);
}


uint8_t *mpngEncoderEncode(WindowContent *wc, DamageReport damageReport, int *dataSize, Bool *reset, void *encoderContext) {
	MpngEncoderContext *ctx = (MpngEncoderContext*) encoderContext;

	int cropWidth;
	int cropHeight;

	// is it the first frame or has the size changed?
	if( ctx->width == 0  ||  (ctx->width != wc->width || ctx->height != wc->height) ) {
		// remember size of WindowContent
		ctx->width = wc->width;
		ctx->height = wc->height;

		// consider everything as damaged - to be on the safe side
		damageReport.damageX = 0;
		damageReport.damageY = 0;
		cropWidth = wc->width;
		cropHeight = wc->height;
		*reset = True;
	} else {
		cropWidth = damageReport.damageWidth;
		cropHeight = damageReport.damageHeight;
		*reset = False;
	}

	// TODO refine cropping

	// crop and convert
	ASSERT(wc->pixelFormat == BGRA_PIXEL_FORMAT);
	uint8_t *rgbImage = convertBGRAtoRGBandCrop(wc->width, wc->height, wc->imageDate, damageReport.damageX, damageReport.damageY, cropWidth, cropHeight);

	// initialize encoder
	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	ASSERT(png_ptr != NULL);

	png_infop info_ptr = png_create_info_struct(png_ptr);
	ASSERT(info_ptr != NULL);

	png_set_IHDR(png_ptr, info_ptr, cropWidth, cropHeight, 8, PNG_COLOR_TYPE_RGB,
			PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

	// output buffer
	PngEncoderBuffer buffer;
	buffer.size = 50000;
	buffer.data = malloc(buffer.size);
	buffer.position = 0;

	// write our cropping info at the beginning
	writeInt(damageReport.damageX, &buffer);
	writeInt(damageReport.damageY, &buffer);

	// write data
	png_set_write_fn(png_ptr, &buffer, pngWriteData, NULL);
	png_write_info(png_ptr, info_ptr);
	png_byte *row_pointers[cropHeight];
	for (int i = 0; i < cropHeight; i++) {
		row_pointers[i] = rgbImage + (cropWidth * 3 * i);
	}
	png_write_image(png_ptr, row_pointers);
	png_write_end(png_ptr, NULL);

	// cleanup
	free(rgbImage);
	png_destroy_write_struct(&png_ptr, &info_ptr);

	*dataSize = buffer.position;
	return buffer.data;
}


void mpngEncoderShutdown(void *encoderContext) {
	MpngEncoderContext *ctx = (MpngEncoderContext*) encoderContext;
	free(ctx);
}
