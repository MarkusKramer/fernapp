#ifndef X11WINDOWDATAMANAGER_H_
#define X11WINDOWDATAMANAGER_H_

/**
 * Manages all open windows.
 * Is implemented thread-safe and provides a public mutex for use by clients to guarantee a consistent view of the window data.
 * All data of returned windowData structs must be read and written within such a mutex!
 *
 * Some thoughts about concurrency:
 *  - Changing window data in the array and sending the corresponding RAUP event must be a atomic operation (using this mutex).
 *    Only then can the addClient method obtain a completely consistent view of the window data, without wrong events being send to the client.
 *  - The RaupServer must deliver events non-blocking because an expensive mutex is locked. But only to the RAUP connections that were valid during this mutex!
 *  - Place window data mangement into the Java part? JNI would cause much overhead, difficulty of hiding X11 data in higher level data structure.
 *  - Does a mutex work for Java threads and native POSIX threads? Yes, on Linux, unsure about Windows.
 *    http://stackoverflow.com/questions/1888160/distinguish-java-threads-and-os-threads
 *  - Mutex for the window array and each individual window? More complex and we usually only have 1-3 windows. Could be slower.
 */

#include "common.h"
#include "../jni/jniConnector.h"


void initWindowDataManagement();

void shutdownWindowDataManagement();

void lockWindowData();

void unlockWindowData();

WindowData *addWindowData(WindowId windowId);

/**
 * Removes the window data from the collection and returns a copy of the removed data.
 * All referenced data and the returned WindowData struct must be freed by the caller AFTER this call.
 * Never returns NULL.
 */
WindowData *removeWindowData(WindowId windowId);

/**
 * Returns the WindowData struct for the specified window. Arguments with a value of 0 will be ignored.
 * All data of returned windowData struct must be read and written within a mutex!
 * Returns NULL if the window was not found.
 */
WindowData *getWindowData(WindowId windowId, WindowId innerWindowId, unsigned long pixmap);

/**
 * Convience function of getWindowData(windowId,0,0). Is fully thread-safe.
 * Returns 0 if the window was not found.
 */
WindowId getInnerWindowId(WindowId windowId);

WindowId getWindowIdByPixmap(unsigned long pixmap);

Bool isWindowKnown(WindowId windowId);

/**
 * Copies the data from a WindowData struct into the corresponding java object.
 */
jobject buildWindowSettingsObject(JNIEnv *env, WindowData *wd);

/**
 * Returns a java ArrayList containing all open windows using JNI.
 */
jobject getOpenWindows(JNIEnv *env);


#endif /* X11WINDOWDATAMANAGER_H_ */
