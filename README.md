# Accessing Linux desktop applications in a web browser

Fernapp makes it possible to run a GUI application (LibreOffice, Evolution, etc) on a Linux server and access it from anywhere via web browser!

It's not an ordinary remote desktop solution. It focuses on the application windows and integrates them nicely.

![screenshot](http://575511713276-website.s3-website-eu-west-1.amazonaws.com/fernapp-files/teaser.png)

## Demo video

[Using LibreOffice in a web browser (demo video)](http://575511713276-website.s3-website-eu-west-1.amazonaws.com/fernapp-files/demo.mp4)

## Features

* Makes LibreOffice remotely accessible via web browser!
* Windows can be moved and positioned arbitrarily and independent of the real application running on the server. It feels as if you're running a locally installed application.
* Runs stand-alone too
* Works with most Linux/X11 applications
* Collaboration! Users can work together on the same application (e.g. LibreOffice document))

## Current state of the project

There hasn't been active development on this project for a while.
I would still like to continue this project, however at this point it requires some major technical overhaul (Wayland support and a modern web client).

Please contact me if you like to contribute.

## Getting Started and documentation

You can get started with fernapp very quickly. Just [follow these instructions](https://fernapp.atlassian.net/wiki/pages/viewpage.action?pageId=7667716).

The complete documentation can be found [here](https://fernapp.atlassian.net/wiki/display/FERN).
