#!/bin/bash

killall -q Xvfb

set -e

# enable core dumps for debugging
ulimit -c unlimited

echo "Starting fernapp server"
JAVA_VERSION=`java -version 2>&1`
echo System: `uname -a`
echo Java version: $JAVA_VERSION

LINUX_ARCH=`uname -i`
if [[ "$LINUX_ARCH" == *"64"*  &&  "$JAVA_VERSION" != *"64-Bit"*  ]]; then
  echo; echo "You have a 64-bit system - please use a 64-bit Java Runtime Environment"
  exit
fi

unzip -n -q -d web fernapp-ua-swing-web.zip

export LD_LIBRARY_PATH=./lib
JAVA_OPTS="-Djava.library.path=$LD_LIBRARY_PATH -Dfernapp.passphrase=123"
# add -Xcheck:jni for more JNI asserts
 
SERVER_JAR="fernapp-server-jar-with-dependencies.jar"

DISPLAY=:5 java $JAVA_OPTS -jar $SERVER_JAR "$@"
