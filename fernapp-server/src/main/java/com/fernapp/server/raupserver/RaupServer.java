package com.fernapp.server.raupserver;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.handshake.ClientGreeting;
import com.fernapp.raup.windowmanagement.server.WindowDestroyed;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.server.encoding.DamageReport;
import com.fernapp.server.executor.ApplicationExecutor;
import com.fernapp.server.executor.WindowEventListener;
import com.fernapp.server.executor.X11ApplicationExecutor;
import com.fernapp.server.executor.X11.ProcessManager;
import com.fernapp.server.launcher.AppLauncher;
import com.fernapp.server.main.connectionservice.ClientConnectionService;
import com.fernapp.server.main.connectionservice.TcpClientConnectionService;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.measurement.Measurement;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.util.Callback;
import com.fernapp.util.NamedThreadFactory;
import com.google.common.base.Preconditions;

/**
 * A RAUP server. Manages the RAUP connections of all clients of this RAUP session.
 * <p>
 * Implements {@link WindowEventListener} to collect events from the underlying {@link ApplicationExecutor} and forwards
 * these events to all registered {@link ServerRaupConnection}s.
 * 
 * @author Markus
 */
public class RaupServer implements WindowEventListener {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private final AnalyticsCollector analyticsCollector;
	private final ApplicationExecutor applicationExecutor;
	private final AppLauncher appLauncher;
	private final ClientConnectionService clientConnectionService;
	// CopyOnWrite makes it unnecessary to use locks which can cause dead locks
	private final List<ServerRaupConnection> serverRaupConnections = new CopyOnWriteArrayList<ServerRaupConnection>();
	private final Executor eventDistributionExecutor;
	private boolean active = false;
	private boolean firstWindow = true;

	public RaupServer(int port, String runtimeDir, String[] applicationCommand, AnalyticsCollector analyticsCollector) {
		Preconditions.checkArgument(applicationCommand.length >= 1);

		this.analyticsCollector = analyticsCollector;
		this.applicationExecutor = new X11ApplicationExecutor(runtimeDir);
		this.appLauncher = new AppLauncher(this, applicationCommand);
		this.clientConnectionService = new TcpClientConnectionService(port);
		this.eventDistributionExecutor = Executors.newFixedThreadPool(1, new NamedThreadFactory(
				"eventDistributionExecutor"));
		analyticsCollector.reportEvent("server", "start", applicationCommand[0]);
	}

	public synchronized void start() {
		Preconditions.checkState(!active);
		active = true;

		log.info("Starting RAUP server");

		// start X server backend
		applicationExecutor.init((WindowEventListener) this, new MeasurementMulticast());

		// start application
		new Thread(appLauncher, "AppLauncher").start();

		// accept client connections
		clientConnectionService.setConnectListener(new ClientConnectionService.ConnectListener() {
			@Override
			public void onConnected(MessagingConnection messagingConnection) {
				startClientHandshake(messagingConnection);
			}
		});
		clientConnectionService.start();
	}

	public synchronized void stop() {
		Preconditions.checkState(active);
		active = false;

		log.info("RAUP server is shutting down");

		clientConnectionService.stop();

		for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
			try {
				serverRaupConnection.close();
			} catch (Exception e) {
				log.error("Failed to close connection to client", e);
			}
		}
		serverRaupConnections.clear();

		appLauncher.disable();

		applicationExecutor.shutdown();

		// stop started processes (including Xvfb) - thus all dependent X connection must have been closed already
		ProcessManager.INSTANCE.stopAll();

		analyticsCollector.reportEvent("server", "stop", "");
		log.info("RAUP server has been shut down");
	}

	public ServerRaupConnection startClientHandshake(final MessagingConnection messagingConnection) {
		Preconditions.checkState(active);

		final ServerRaupConnection raupConnection = new DefaultServerRaupConnection(messagingConnection,
				applicationExecutor, analyticsCollector);

		log.info("Handshaking... Waiting for greeting from client");
		messagingConnection.registerReceivedHandler(ClientGreeting.class, new Callback<ClientGreeting>() {
			@Override
			public void onCallback(final ClientGreeting clientGreeting) throws Exception {
				Runnable clientSetup = new Runnable() {
					public void run() {
						try {
							boolean accepted = raupConnection.processHandshake(clientGreeting);
							if (accepted) {
								addClient(raupConnection);
							}
						} catch (Exception e) {
							log.error("RAUP setup has failed", e);
							raupConnection.close();
						}
					}
				};
				new Thread(clientSetup, messagingConnection.getName() + "Setup").start();
			}
		});
		messagingConnection.startReceivingMessages();

		return raupConnection;
	}

	/**
	 * Adds a new RAUP connection to the manager.
	 */
	private void addClient(ServerRaupConnection raupConnection) throws IOException {
		// there is a delay until events are delivered to the new connection. A sync on
		// the windows data (and all event processing methods) will guarantee a
		// consistent view for the ServerRaupConnection during initialization (see
		// raupConnection#initialize)

		applicationExecutor.lockWindowData();
		try {
			raupConnection.initialize();
			// potential concurrency problem: there could be queued old events - maybe
			// block until task queue is empty
			serverRaupConnections.add(raupConnection);
		} finally {
			applicationExecutor.unlockWindowData();
		}

		// can be done outside the synchronization block
		raupConnection.startUsage();
	}

	public void onWindowSettingsChange(final WindowSettings windowSettings, final boolean isNewWindow) {
		try {
			eventDistributionExecutor.execute(new Runnable() {
				public void run() {
					if (log.isDebugEnabled()) {
						log.debug("onWindowSettingsChange: " + (isNewWindow ? "NEW " : "") + windowSettings.toString());
					}

					if (firstWindow) {
						logServerRunning();
						firstWindow = false;
					}

					try {
						for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
							if (serverRaupConnection.isOpen()) {
								serverRaupConnection.onWindowSettingsChange(windowSettings, isNewWindow);
							} else {
								serverRaupConnections.remove(serverRaupConnection);
							}
						}
					} catch (Exception e) {
						log.error("onWindowSettingsChange failed", e);
					}
				}
			});
		} catch (Exception e) {
			log.error("onWindowSettingsChange task submission failed", e);
		}
	}

	public void onWindowDestroyed(final String windowId) {
		try {
			eventDistributionExecutor.execute(new Runnable() {
				public void run() {
					if (log.isDebugEnabled()) {
						log.debug("onDestroyed: windowId " + windowId);
					}

					try {
						WindowDestroyed windowDestroyed = new WindowDestroyed(windowId);
						for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
							if (serverRaupConnection.isOpen()) {
								serverRaupConnection.onWindowDestroyed(windowDestroyed);
							} else {
								serverRaupConnections.remove(serverRaupConnection);
							}
						}
					} catch (Exception e) {
						log.error("onDestroyed failed", e);
					}
				}
			});
		} catch (Exception e) {
			log.error("onDestroyed task submission failed", e);
		}
	}

	@Override
	public void onWindowContentUpdate(final String windowId, final DamageReport damageReport) {
		try {
			eventDistributionExecutor.execute(new Runnable() {
				public void run() {
					if (log.isTraceEnabled()) {
						log.trace("onWindowContentUpdate: windowId " + windowId);
					}

					try {
						for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
							if (serverRaupConnection.isOpen()) {
								serverRaupConnection.onWindowContentUpdate(windowId, damageReport);
							} else {
								serverRaupConnections.remove(serverRaupConnection);
							}
						}
					} catch (Exception e) {
						log.error("onWindowContentUpdate failed", e);
					}
				}
			});
		} catch (Exception e) {
			log.error("onWindowContentUpdate task submission failed", e);
		}
	}

	/**
	 * Sends measurements to all RAUP connections.
	 */
	private class MeasurementMulticast implements MeasurementReceiver {

		/**
		 * @see com.fernapp.uacommon.measurement.MeasurementReceiver#receiveMeasurement(com.fernapp.uacommon.measurement.DelayCategory,
		 *      long, long)
		 */
		public void receiveMeasurement(DelayCategory delayCategory, long delay, long dateSize) {
			Measurement measurement = new Measurement(System.currentTimeMillis(), delayCategory, delay, dateSize);
			receiveMeasurement(measurement);
		}

		/**
		 * @see com.fernapp.uacommon.measurement.MeasurementReceiver#receiveMeasurement(com.fernapp.uacommon.measurement.Measurement)
		 */
		public void receiveMeasurement(Measurement measurement) {
			for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
				if (serverRaupConnection.isOpen()) {
					serverRaupConnection.getMeasurementReceiver().receiveMeasurement(measurement);
				} else {
					serverRaupConnections.remove(serverRaupConnection);
				}
			}
		}

	}

	private void logServerRunning() {
		String s = "\n\n";
		s += "=========================================================================\n";
		s += "   fernapp server is RUNNING and the application has started\n";
		s += "=========================================================================\n";
		log.info(s);
	}

}
