package com.fernapp.server.raupserver;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnalyticsCollector {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private final String trackingId = "UA-16584825-6";
	private final String clientId;
	private final boolean enabled;
	private final ExecutorService executor = Executors.newCachedThreadPool();

	public AnalyticsCollector(boolean enabled) {
		this.clientId = createClientId();
		this.enabled = enabled;

		if (enabled) {
			log.info("Anonymous collection of usage data is enabled");
		} else {
			log.info("Anonymous collection of usage data is DISABLED :-( We would really appreciate your feedback though");
		}
	}

	public void reportEvent(final String category, final String action, final String label) {
		if (enabled) {
			executor.submit(new Runnable() {
				@Override
				public void run() {
					try {
						sendEvent(category, action, label);
					} catch (IOException e) {
						log.debug("Could not send analytics event - this is nothing to worry about ({})", e.toString());
					}
				}
			});
		}
	}

	protected void sendEvent(String category, String action, String label) throws IOException {
		// https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#event
		// Event Components: https://support.google.com/analytics/answer/1033068
		String body = String.format("v=1&tid=%s&cid=%s&t=event&ec=%s&ea=%s&el=%s&ev=%s", //
				trackingId, clientId, category, action, label, "0");

		log.trace("Sending analytics event: {}", body);

		String url = "http://www.google-analytics.com/collect";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "Java");
		con.setDoOutput(true);
		DataOutputStream out = new DataOutputStream(con.getOutputStream());
		out.writeBytes(body);
		out.flush();
		out.close();

		int responseCode = con.getResponseCode();
		if (responseCode != 200) {
			throw new RuntimeException("Request failed");
		}
	}

	protected String createClientId() {
		String clientId = null;

		try {
			Enumeration<NetworkInterface> iter = NetworkInterface.getNetworkInterfaces();
			while (clientId == null && iter.hasMoreElements()) {
				NetworkInterface networkInterface = iter.nextElement();

				byte[] macBytes = networkInterface.getHardwareAddress();
				if (macBytes != null) {
					String macAddress = DatatypeConverter.printHexBinary(macBytes);
					log.trace("MAC address is {}", macAddress);
					clientId = String.valueOf(macAddress.hashCode());
				}
			}
		} catch (Exception e) {
			log.error("Failed to retrieve MAC address", e);
			clientId = "unknown";
		}

		log.info("Using client ID {}", clientId);
		return clientId;
	}

}
