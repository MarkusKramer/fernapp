package com.fernapp.server.launcher;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.server.executor.X11.ProcessManager;
import com.fernapp.server.raupserver.RaupServer;

public class AppLauncher implements Runnable {

	private final Logger log = LoggerFactory.getLogger(getClass());
	private final RaupServer raupServer;
	private final String[] applicationCmd;
	private volatile boolean active = true;

	public AppLauncher(RaupServer raupServer, String[] applicationCmd) {
		this.raupServer = raupServer;
		this.applicationCmd = applicationCmd;
	}

	@Override
	public void run() {
		try {
			while (active) {
				boolean success = runApp();

				if (active) {
					String msg;
					if (success) {
						msg = "The application has been closed. Restart it?";
					} else {
						msg = "The application seems to have been close due to an error Restart it?";
					}

					int answer = JOptionPane.showConfirmDialog(null, msg, "Restart", JOptionPane.YES_NO_OPTION);
					if (answer != JOptionPane.YES_OPTION) {
						raupServer.stop();
					}
				}
			}

		} catch (IOException e) {
			log.error("Failed launch app", e);
		} catch (InterruptedException e) {
			log.warn("We have been interrupted - maybe due to shutdown of the RAUP server");
		}
	}

	private boolean runApp() throws IOException, InterruptedException {
		log.info("Starting " + applicationCmd[0]);
		Process process = ProcessManager.INSTANCE.startProcess(LoggerFactory.getLogger("com.fernapp.server.App"),
				applicationCmd);

		try {
			int returnValue = process.waitFor();
			log.info("Process has ended with return value " + returnValue);
			return returnValue == 0;
		} catch (InterruptedException e) {
			process.destroy();
			throw new InterruptedException();
		}
	}

	public void disable() {
		this.active = false;
	}

}
