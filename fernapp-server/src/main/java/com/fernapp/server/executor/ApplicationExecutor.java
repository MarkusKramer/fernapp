package com.fernapp.server.executor;

import java.util.Collection;

import com.fernapp.raup.windowmanagement.client.InputEvent;
import com.fernapp.raup.windowmanagement.client.WindowResizeRequest;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.server.encoding.WindowContent;
import com.fernapp.uacommon.measurement.MeasurementReceiver;

/**
 * Executes the application and encapsulates access to the native window manager. Delivers input events and reports
 * window events to the registered listener.
 * 
 * @author Markus
 */
public interface ApplicationExecutor {

	void init(WindowEventListener windowEventListener, MeasurementReceiver measurementReceiver);

	/**
	 * Captures the current window content. Returns NULL if the window does not exist or some other error occured.
	 */
	WindowContent captureWindowContent(String windowId);

	/**
	 * Delivers an input event to the specified window.
	 */
	void sendInputEvent(InputEvent inputEvent);

	/**
	 * Send a resize request to the specified window. Changes to the window size might to be immediately reflected in by
	 * the output of {@link #captureWindowContent(String)}.
	 */
	void requestWindowResize(WindowResizeRequest windowResizeRequest);

	/**
	 * Requests that the window be closed.
	 */
	void requestWindowClose(String windowId);

	/**
	 * Returns all opened windows.
	 */
	Collection<WindowSettings> getOpenWindows();

	/**
	 * Locking the window data allows getting a consistent view of all opened windows with {@link #getOpenWindows()}.
	 */
	void lockWindowData();

	void unlockWindowData();

	void shutdown();

}
