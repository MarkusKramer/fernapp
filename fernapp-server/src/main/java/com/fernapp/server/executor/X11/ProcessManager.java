package com.fernapp.server.executor.X11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;

/**
 * Manages background processes.
 * 
 * @author Markus
 */
public enum ProcessManager {

	INSTANCE;

	private final Logger log = LoggerFactory.getLogger(getClass());
	private final Deque<ProcessInfo> processes = new LinkedList<ProcessInfo>();
	private final Executor executor = Executors.newCachedThreadPool();

	public void stopAll() {
		synchronized (processes) {
			for (ProcessInfo p : processes) {
				log.info("Stopping process " + p.getName());
				try {
					p.getProcess().destroy();
					p.getProcess().waitFor();
					Thread.sleep(500);
				} catch (Throwable e) {
					log.error("Failed to stop process", e);
				}
			}
			processes.clear();
		}
	}

	/**
	 * Executes the given command as a new command. Does not block. Logs the process output in a new thread.
	 */
	public Process startProcess(final Logger logger, String... command) throws IOException {
		if (log.isDebugEnabled()) {
			log.debug("Starting process: " + Joiner.on(" ").join(command));
		}

		ProcessBuilder builder = new ProcessBuilder(command);
		builder.redirectErrorStream(true);
		builder.environment().put("DISPLAY", System.getenv("DISPLAY"));
		final Process process = builder.start();

		synchronized (processes) {
			processes.push(new ProcessInfo(command[0], process));
		}

		executor.execute(new Runnable() {
			public void run() {
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					String line;
					while ((line = reader.readLine()) != null) {
						logger.info(line);
					}
					reader.close();
				} catch (IOException e) {
					// ignore - happens when stream gets closed
				} catch (Exception e) {
					log.error("Reading process output failed", e);
				}
			}
		});

		return process;
	}

	public static class ProcessInfo {
		private String name;
		private Process process;

		public ProcessInfo(String name, Process process) {
			this.name = name;
			this.process = process;
		}

		public String getName() {
			return name;
		}

		public Process getProcess() {
			return process;
		}
	}

}
