package com.fernapp.server.encoding;

import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;

/**
 * Responsible for encoding the window content. Correctly timed play back or frame rate is of no importance. The chosen
 * quality must be between 0 and 100 and must influence the size of {@link VideoStreamChunk}s. Is completely
 * thread-safe. Implementations use a {@link WindowContentProvider} to obtain the window content.
 * <p>
 * Upon creation of a new encoder, the {@link #getEncodedChunk()} method with return the initial window content (there
 * is need to call {@link #onWindowUpdated(DamageReport)}.
 * 
 * @author Markus
 */
public interface Encoder {

	/**
	 * The window this encoder is responsible for. Actually not really relevant for an encoder to know this, but a handy
	 * extra information.
	 */
	String getWindowId();

	void changeQuality(int quality);

	/**
	 * Will be called if the window content has changed. Size of window may change between calls. Gives the encoder the
	 * chance to do background encoding. Call must not block.
	 */
	void onWindowUpdated(DamageReport damageReport);

	/**
	 * Called to get an encoded stream chunk. Returns the latest chunk of encoded video data for this window. Returns
	 * NULL if there is nothing new, no window content could be obtained (because window is gone), or shutdown has been
	 * called.
	 * <p>
	 * How the input data is obtained is implementation dependent. A simple implementation would block and encode the
	 * latest image. A better implementation will encode the latest content in advance.
	 */
	VideoStreamChunk getEncodedChunk();

	/**
	 * Encoder shutdown. Frees all allocates resources.
	 */
	void shutdown();

}
