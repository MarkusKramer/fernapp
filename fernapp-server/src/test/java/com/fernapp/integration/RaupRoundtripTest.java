package com.fernapp.integration;

import java.awt.event.MouseEvent;
import java.util.concurrent.TimeUnit;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.server.executor.X11ApplicationExecutorTest;
import com.fernapp.server.raupserver.AnalyticsCollector;
import com.fernapp.server.raupserver.DefaultServerRaupConnection;
import com.fernapp.server.raupserver.RaupServer;
import com.fernapp.server.raupserver.ServerRaupConnection;
import com.fernapp.uacommon.measurement.AggregatingMeasurementProcessor.MeasurementReport;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.middleware.JavaSerMessagingConnection;
import com.fernapp.uacommon.middleware.LoopbackConnection;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.uacommon.middleware.channel.DataChannel;
import com.fernapp.uacommon.middleware.channel.SocketDataChannel;
import com.fernapp.useragent.AbstractRaupClient;
import com.fernapp.useragent.RaupClient;
import com.fernapp.useragent.common.ActionDelayMeasurer;
import com.fernapp.useragent.ui.RemoteWindowController;
import com.google.common.base.Preconditions;

/**
 * Simulate a complete usage roundtrip of the RAUP protocol. Allows network latency analysis.
 * 
 * Adding packet latency:
 * 
 * <pre>
 * tc qdisc show dev lo
 * tc qdisc add dev lo root netem delay 100ms
 * tc qdisc del dev lo root
 * 
 * http://www.linuxpoweruser.com/?p=41
 * </pre>
 * 
 * We use the RAW encoder. Requires a large TCP receive buffer, to prevent misleading results:
 * 
 * <pre>
 * Set maximum size of TCP receive window
 * echo 400000 > /proc/sys/net/core/rmem_max
 * Set maximum size of TCP transmit window
 * echo 400000 > /proc/sys/net/core/wmem_max
 * </pre>
 * 
 * @author Markus
 * 
 */
public class RaupRoundtripTest {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Test
	@Ignore
	public void roundtrip() throws Exception {
		log.debug("------------------------------------ Starting roundtrip test");
		DefaultServerRaupConnection.passphrase = "TEST";
		final LoopbackConnection lp = new LoopbackConnection();

		// initialize client
		final MockWindowContainer windowContainer = new MockWindowContainer();
		final RaupClient raupClient = new AbstractRaupClient() {
			protected RemoteWindowController createRemoteWindowController(WindowSettings windowSettings) {
				// we only support one window
				Preconditions.checkState(getWindows().isEmpty());
				return new RemoteWindowController(windowSettings.getWindowId(), windowContainer, this);
			}

			protected void showMessage(String message) {
				log.info("RAUP client says:" + message);
			}

			protected String getPassphrase() {
				return DefaultServerRaupConnection.passphrase;
			}
		};
		Thread clientConnectThread = new Thread(new Runnable() {
			public void run() {
				try {
					MessagingConnection clientMessagingConnection = new JavaSerMessagingConnection(
							new SocketDataChannel(lp.getClientSocket()), "raup");
					raupClient.initForConnection(clientMessagingConnection);
					clientMessagingConnection.startReceivingMessages();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		clientConnectThread.start();

		log.info("------------------------------------ Initializing server");
		final RaupServer raupServer = new RaupServer(4711, X11ApplicationExecutorTest.RUNTIME_DIR,
				X11ApplicationExecutorTest.TEST_APP_COMMAND, new AnalyticsCollector(false));
		raupServer.start();
		DataChannel dataChannel = new SocketDataChannel(lp.getServerSocket());
		MessagingConnection messagingConnection = new JavaSerMessagingConnection(dataChannel, "raup");
		ServerRaupConnection serverRaupConnection = raupServer.startClientHandshake(messagingConnection);

		try {
			log.info("------------------------------------ Waiting for client to get ready");
			clientConnectThread.join();
			while (raupClient.getWindows().isEmpty() || windowContainer.getInputEventListener() == null
					|| serverRaupConnection.getState() != ServerRaupConnection.State.ACTIVE) {
				TimeUnit.MILLISECONDS.sleep(100);
			}
			long packetDelay = serverRaupConnection.getQualityFeedbackService().getPacketDelay();
			long minWaitTime = 500 + (packetDelay * 4);

			log.info("------------------------------------ Setting mouse position and warm up");
			windowContainer.getInputEventListener().mouseMoved(50, 50);
			performClick(windowContainer);
			TimeUnit.MILLISECONDS.sleep(minWaitTime);
			serverRaupConnection.getMeasurementProcessor().reset();
			ActionDelayMeasurer.INSTANCE.reset();

			log.info("------------------------------------ ROUNDTRIP TEST");
			int repetitions = 3;
			for (int i = 0; i < repetitions; i++) {
				// trigger input event through client
				performClick(windowContainer);
				TimeUnit.MILLISECONDS.sleep(minWaitTime);
			}

			// measurements results need to get send from the client to the server
			TimeUnit.MILLISECONDS.sleep(minWaitTime);

			log.info("------------------------------------ Evaluation");
			serverRaupConnection.getMeasurementProcessor().logMeasurements();
			int partSum = 0;
			for (DelayCategory delayCategory : DelayCategory.values()) {
				MeasurementReport mr = serverRaupConnection.getMeasurementProcessor().generateReport(delayCategory);
				if (mr != null) {
					partSum += mr.getAvgDelay();
				}
			}
			long discrepancy = ActionDelayMeasurer.INSTANCE.getAvgDelay() - partSum - packetDelay;
			String discrepancyMsg = String.format(
					"Discrepancy: %d (Action Delay) - %d (Part Sum) - %d (Input Msg) = %d",
					ActionDelayMeasurer.INSTANCE.getAvgDelay(), partSum, packetDelay, discrepancy);
			log.info(discrepancyMsg);
		} finally {
			// shutdown
			log.info("------------------------------------ Shutdown");
			raupClient.shutdown();
			raupServer.stop();
			lp.close();
		}
	}

	private void performClick(MockWindowContainer windowContainer) throws InterruptedException {
		log.info("Performing click");
		windowContainer.getInputEventListener().mousePressed(MouseEvent.BUTTON1);
		TimeUnit.MILLISECONDS.sleep(100);
		windowContainer.getInputEventListener().mouseReleased(MouseEvent.BUTTON1);
	}
}
